.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../../../definitions.rst

Release Notes
#############

About
*****

The objective of this document is to provide basic introductory information
about included functionalities, known issues and instructions guidance for the
2.0.0 release of the |main_project_name| project.

The |main_project_name| project serves as a solid base foundation for products.
It is not a standalone product itself but rather a platform that aims at
accelerating the development and maintenance of other projects and products.

During this release, the project's code and most of its infrastructure moved to
Eclipse Foundation as one of the top-level projects. This follows the fact that
|main_project_name| became an Eclipse Foundation project during a preview
release iteration.

Scope
*****

The Objectives of the Release
-----------------------------

The objective of the *2.0.0* release is to consolidate the features previously
released, add new outstanding features and meet the requirements defined by the
Eclipse Foundation Development Process. That includes, but is not limited to:

- Migration of code to Eclipse Foundation
- Work towards migration of the project's infrastructure to Eclipse Foundation
  (eg.  CI, pipelines, etc.)
- Hardware & Software roadmap expansion
- Deployment of the IP compliance toolchain and related policies
- Security and maintenance featuring CVE fixes, LEDGE support, etc.

The List of Software Features Included
--------------------------------------

- Linux Kernel LTS version supported for all ARM and X86 targets
- Zephyr kernel 3.1 supported for all ARM and X86 targets
- Over-the-Air system update, including integration with hawkbit and SysOTA
  backends
- Toolchain package upgrades including LLVM, GCC, musl, etc.
- Matter 1.0 and Thread 1.2 support
- Modbus support with RTU (over RS485) and TCP modes
- New hardware supported: Raspberry Pi 3, SECO (SBC-C61, SBC-B68, SBC-D23),
  QEMU ARM 32/64bit, Arduino Nano 33 BLE
- Development and integration of functional testing suites
- Integration of standard test toolkits (eg. LTP)
- More LAVA support development including new LAVA nodes
- Security OS features targeting runtime and build-time (:ref:`SecurityGuide`)
- Development for security-oriented features for Oniro upgrades and CVE fixes
- Upstream contributions in terms of fixes, backports and CVE issues
- Repackaging of |main_project_name| from monolithic to modular, leveraging the
  Yocto Project layer architecture, as well as the Eclipse Foundation
  sub-project structure
- Split Blueprints, Sysota, and OpenHarmony components into their sub-project

For more details, a full list of requiremnts can be seen
:ref:`here<2_0_0_Requirements>`.

Supported Hardware Platforms
----------------------------

+---------------------------------+-------------------+---------------------------------------+
| Board (chipset)                 | Supported kernels | Board documentation                   |
+=================================+===================+=======================================+
| QEMU X86                        | Linux & Zephyr    | :ref:`SupportedVBoardQemuX86`         |
+---------------------------------+-------------------+---------------------------------------+
| QEMU X86-64                     | Linux             | :ref:`SupportedVBoardQemuX86-64`      |
+---------------------------------+-------------------+---------------------------------------+
| QEMU ARM (EFI)                  | Linux             | :ref:`SupportedVBoardQemuARM`         |
+---------------------------------+-------------------+---------------------------------------+
| QEMU ARM64 (EFI)                | Linux             | :ref:`SupportedVBoardQemuARM64`       |
+---------------------------------+-------------------+---------------------------------------+
| QEMU Cortex M3                  | Zephyr            | :ref:`Zephyr_Kernel`                  |
+---------------------------------+-------------------+---------------------------------------+
| SECO SBC-B68                    | Linux             | :ref:`SupportedBoardSecoB68`          |
+---------------------------------+-------------------+---------------------------------------+
| SECO SBC-C61                    | Linux             | :ref:`SupportedBoardSecoC61`          |
+---------------------------------+-------------------+---------------------------------------+
| SECO SBC-D23                    | Linux             | :ref:`SupportedBoardSecoD23`          |
+---------------------------------+-------------------+---------------------------------------+
| Raspberry Pi 4 B (BCM2711)      | Linux             | :ref:`raspberrypi`                    |
+---------------------------------+-------------------+---------------------------------------+
| Raspberry Pi 3 B+ (BCM2837B0)   | Linux             | :ref:`raspberrypi`                    |
+---------------------------------+-------------------+---------------------------------------+
| Arduino Nano 33 BLE             | Zephyr            | :ref:`SupportedBoardArduinoNano33BLE` |
+---------------------------------+-------------------+---------------------------------------+

Installation
************

:ref:`Quick Build <OniroQuickBuild>` provides an example of how to build the
|main_project_name| project for an example target. Visit the :ref:`Hardware
Support <HardwareSupport>` section for instructions on how to build for other
supported targets.

Visit :ref:`setting up a repo workspace <RepoWorkspace>` for instructions how
to prepare the workspace for development. Since |main_project_name| uses `repo`
tool for its development, you can use the release tag for the `repo init`
commands as follows:

.. code-block:: console

    repo init -u https://gitlab.eclipse.org/eclipse/oniro-core/oniro.git -b v2.0.0

Known Issues
------------

Visit `this link
<https://gitlab.eclipse.org/groups/eclipse/oniro-core/-/issues/?sort=label_priority_desc&state=all&label_name%5B%5D=KnownIssue%3A%3ARelease2.0.0&first_page_size=20>`__
to see known issues for this release.

Source Code
-----------

For more details on our repo structure, see `OniroProject's GitLab
<https://gitlab.eclipse.org/eclipse/oniro-core>`__ project group.

DevOps Infrastructure
*********************

To learn more about our approach to CI (Continuous Integration) strategy used
for this release, please see:

:doc:`/ci/index` document.

Testing
-------

Details can be found in:

:doc:`/ci/device-testing` document.

This release comes with a detailed `Test Report` accesible
:ref:`here<2_0_0_TestReport>`.

IP Compliance
-------------

This release comes with a detailed `IP Compliance note` accesible
:ref:`here<2_0_0_IPComplianceNote>`.

Security Status
---------------

This release comes with a detailed `Security report` with a list of known CVEs
accessible :ref:`here<2_0_0_SecurityReport>`.

As part of the Oniro continuous compliance process, all relevant metadata about
compliance and security are collected at build time for every commit to the
Oniro project repo, and can be inspected through a `dedicated dashboard`_. Open
CVEs on single components data may be inspected by filtering components by
status ("has unpatched CVEs") and by opening the component details: open CVEs
are displayed first, marked in red and ordered by score, while closed CVEs are
marked in green. The dashboard gets updated after every commit and contains a
CVE status snapshot at the time of the commit. However, if some previously open
CVEs are fixed, this would be visible by inspecting the component's previous
variants in the dashboard.

.. _dedicated dashboard: https://sca.software.bz.it/?json=https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/mirrors/oniro-goofy/-/jobs/artifacts/kirkstone/raw/report.harvest.json?job=harvest

Out of Scope
************

|main_project_name| bundles components from multiple upstream sources. These
upstream sources come with features that the project might not explicitly
mention as part of its supportability policy. Similarly, |main_project_name|
includes supported features that are explicitly marked as 'experimental'. All
these cases, mainly driven by the community, will imply basic support from the
project.

|main_project_name| exercises an upstream-first support policy, so even for
cases that are not strictly in the supportability scope of the project,
|main_project_name| will provide guidance and traceability while facilitating
the upstream effort.

Contributions
*************

If you are a developer eager to know more details about |main_project_name| or
just an enthusiast with a patch proposal, you are welcome to participate in our
|main_project_name| ecosystem development. To do so, please sign-up using the
process described below:

:doc:`/contributing/index` document.

License
*******

Project manifest, project-specific meta-layers, recipes, and software packages
are published under the MIT license unless specified otherwise. The whole
operating system built by users from the project manifest is an aggregate
comprised of many third-party components or component groups, each subject to
its license conditions.

Official project release includes only the project manifest, project-specific
meta-layers, and recipes. Any reference binary image, build cache, and other
build artefacts are distributed only as a convenience and are not part of the
release itself.

.. note::

   "supported" `referred to a board` means that a board is officially targeted
   as a potential platform where an Oniro image can be installed for any
   purposes; when `referred to an image`, means that the image targeting a
   supported board receives thorough testing and specific attention during the
   development. It does NOT mean that both will receive support services nor
   that any member of the Oniro Working Group or of the Eclipse Foundation will
   provide any warranty whatsoever.
