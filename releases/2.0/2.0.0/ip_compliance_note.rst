.. SPDX-FileCopyrightText: Alberto Pianon <pianon@array.eu> and Carlo Piana <piana@array.eu>
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _2_0_0_IPComplianceNote:

IP Compliance Note
==================

Since the very beginning, a Continuous Compliance `toolchain`_ and `process`_
have been developed and integrated into the Oniro project development so that
source components used to generate Oniro binary images are continuously scanned
by open-source tools like `Fossology`_ and `Scancode`_, and reviewed by
Software Audit Experts and IP Lawyers [*]_.

For detailed information about the why and the how of such a process, please
refer to the Oniro Compliance Toolchain’s `official documentation`_. Sources
and documentation for custom components of the toolchain (`tinfoilhat`_,
`aliens4friends`_, `dashboard`_, `pipelines`_) can be found in their respective
repositories.

*TL;DR*: we put ourselves in your shoes, a device maker willing to use Oniro to
develop its products. We simulated the IP compliance work you would have to do
(on third-party components fetched by Yocto recipes) to build your firmware
image(-s) and spot possible legal risks and issues. In the true open-source
spirit, every time we found an issue with a particular upstream component, we
raised that issue upstream, and most of the time we got it solved for you by
upstream developers.

As of Oniro’s 2.0.0 GA Release, there are just a few issues left that we cannot
address. These relate to proprietary firmware/drivers for hardware support and
some patent-covered technologies. The issues require your attention and
possible action, e.g. getting a patent license. We will briefly explain these
here.

The overall status of audit activities can be monitored through a `dedicated
dashboard`_, which gets updated after every commit to Oniro's main repository.
In the dashboard, also CVE information (collected at the time of the commit) is
shown and can be filtered based on target machines, images, and single
components.

All repositories included in the Oniro 2.0.0 Release are `REUSE compliant
<https://reuse.software/spec/>`_. It means that copyright and license metadata
for every source file are made available within each repository in a standard
machine-readable format, and that at any time one can generate an SPDX SBoM
[*]_ for such repositories with `REUSE tool
<https://github.com/fsfe/reuse-tool>`_ by just running ``reuse spdx`` command.
REUSE-generated SPDX files for all released repositories are available as part
of the `release artefacts download area
<https://download.eclipse.org/oniro-core/releases/oniro-v2.0.0_spdx_sbom.tar.gz>`_.

Last but not least, we provide reference SPDX SBoM of source packages used to
build oniro-image-base and zephyr-philosophers images for a selection of
supported target machines (qemu, raspberrypi4, arduino-nano-33ble), generated
by continuous compliance pipelines. They are provided as a convenience only,
with no express or implied warranty about the accuracy and completeness of the
information contained therein (see the disclaimers below):

============================= ====== ============ =================== ===================
SBoM                          kernel toolchain(s) machine(s)          image
============================= ====== ============ =================== ===================
`linux-qemu`_                 linux  gcc,clang    qemu\*              oniro-image-base
`linux-raspberrypi4`_         linux  gcc,clang    raspberrypi4-64     oniro-image-base
`zephyr-qemu`_                zephyr gcc          qemu\*              zephyr-philosophers
`zephyr-arduino-nano-33-ble`_ zephyr gcc          arduino-nano-33-ble zephyr-philosophers
============================= ====== ============ =================== ===================

.. _linux-qemu: https://download.eclipse.org/oniro-core/releases/2.0.0/oniro-v2.0.0_linux-qemu_images_spdx_sbom.zip
.. _linux-raspberrypi4: https://download.eclipse.org/oniro-core/releases/2.0.0/oniro-v2.0.0_linux-raspberrypi4_images_spdx_sbom.zip
.. _zephyr-qemu: https://download.eclipse.org/oniro-core/releases/2.0.0/oniro-v2.0.0_zephyr-qemu_images_spdx_sbom.zip
.. _zephyr-arduino-nano-33-ble: https://download.eclipse.org/oniro-core/releases/2.0.0/oniro-v2.0.0_zephyr-arduino-nano-33-ble_images_spdx_sbom.zip

*Disclaimer#1*: This is not legal advice. This note is provided just as a
convenience for you, to suggest some critical areas in which you should seek
legal advice if you want to develop real-world products based on Oniro. It is
not meant to be complete nor to substitute internal due-diligence activities
you need to perform before marketing your products.

*Disclaimer#2*: This note covers only source components used to generate
supported Oniro images (oniro-image-base and zephyr-philosophers) for supported
target machines (qemux86-64, qemux86, qemuarm-efi, qemuarm64-efi,
raspberrypi4-64, seco-intel-b68, seco-px30-d23, seco-imx8mm-c61-2gb,
seco-imx8mm-c61-4gb, qemu-cortex-m3, nrf52840dk-nrf52840, arduino-nano-33-ble).

*Disclaimer#3*: “supported” *referred to a board* means that a board is
officially targeted as a potential platform where an Oniro image can be
installed for any purposes; when *referred to an image*, means that the image
targeting a supported board receives thorough testing and specific attention
during the development. It does NOT mean that both will receive support
services nor that any member of the Oniro Working Group or of the Eclipse
Foundation will provide any warranty whatsoever.

Solved Issues
-------------

-  There was a proprietary software font accidentally included in
   zephyr-philosophers; we opened the issue upstream
   (https://github.com/zephyrproject-rtos/zephyr/issues/48111), which was
   solved (https://github.com/zephyrproject-rtos/zephyr/pull/49103), and the
   fix was backported to Oniro.
   (https://gitlab.eclipse.org/eclipse/oniro-core/meta-zephyr/-/commit/0f36ae849d59da08e445af83f711a1c0108dd3bf);

-  A similar issue was found also in Harfbuzz component, raised upstream
   (https://github.com/harfbuzz/harfbuzz/issues/3845), fixed
   (https://github.com/harfbuzz/harfbuzz/pull/3846), and the fix was backported
   to Oniro
   (https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/commit/fbb4bc229b287fa293439ee0adbb0d830764b2d8).

-  There were a lot of binary files found in zephyr-philosophers, without
   corresponding sources and no clear license information; we opened the issue
   upstream
   (https://gitlab.eclipse.org/eclipse/oniro-core/meta-zephyr/-/commit/0f36ae849d59da08e445af83f711a1c0108dd3bf),
   which was then fixed
   (https://github.com/zephyrproject-rtos/zephyr/pull/47181), and the fix was
   backported to Oniro.
   (https://gitlab.eclipse.org/eclipse/oniro-core/meta-zephyr/-/commit/a00d1c4f1aad8b0ea5b9f904966c0bd8a48d8d80)

-  Some proprietary license headers, not granting redistribution nor any other
   rights without written permission by Intel, were found in some source files
   in the Intel-Media-SDK component; we opened the issue upstream
   (https://github.com/Intel-Media-SDK/MediaSDK/issues/2937) and it turned out
   it was an oversight occurred when open sourcing the component; it was then
   fixed (https://github.com/Intel-Media-SDK/MediaSDK/pull/2939) and the fix was
   backported to Oniro.
   (https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/commit/d5ee837d90903d91a1ff358ebfe985d28925484e);

-  A similar issue was found also in the Intel-Media-Driver component, it was
   raised upstream (https://github.com/intel/media-driver/issues/1460), fixed
   (https://github.com/intel/media-driver/pull/1465), and the fix was backported
   to Oniro
   (https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/commit/b56de944568c8e348cb8265c59d7cfd52a0831b9)

Warnings for Downstream Users: Hardware Support
-----------------------------------------------

Linux
~~~~~

IMX Firmware
^^^^^^^^^^^^

A couple of supported target boards (seco-imx8mm-c61-2gb and
seco-imx8mm-c61-4gb) require Freescale i.MX firmware for VPU and SDMA as well
as firmware for 8M Mini family to train memory interface on SoC and DRAM during
initialization. These firmware require acceptance of a `EULA`_ by the user
(you). Such acceptance may be provided by flagging a specific environment
variable (``ACCEPT_FSL_EULA = "1"``) in your configuration file (please refer
to Oniro’s technical documentation). You should carefully read that `EULA`_ to
check whether you are actually in a position to accept it and whether you can
fulfill all of its conditions. If needed, seek legal advice for that.

Linux-firmware
^^^^^^^^^^^^^^

The third-party components ``linux-firmware`` and ``linux-firmware-rpidistro``
contain many sub-components (mainly firmware BLOBs) for specific hardware
support, coming from different hardware vendors.

Almost all firmware vendor licenses restrict firmware usage to the specific
device(s) of their own.

Some of them (apparently) contain further restrictions, stating that the binary
file is licensed *“for use with [vendor] devices, but not as a part of the
Linux kernel or in any other form which would require these files themselves to
be covered by the terms of the GNU General Public License”*. Our understanding
is that such restriction is either redundant or useless. Apart from some
debatable and contested corner cases, there is no way in which a firmware blob
may become part of the Linux kernel and therefore be covered by the GNU General
Public License - so the above provision seems redundant. But even if someone
claimed that a proprietary firmware requires such a low-level interaction with
the kernel that such firmware must be deemed as a derivative work of the kernel
itself, such (alleged) non-compliance with GPL could not be avoided or excluded
by a vendor license clause - so the above provision would be useless. You
should seek legal advice to use the affected firmware files in either case.

================================= ================================================== ======================== ============================
Source                            Device/driver                                      File(s)                  License found in
================================= ================================================== ======================== ============================
`linux-firmware-20220913.tar.xz`_ Conexant Cx23100/101/102 USB broadcast A/V decoder v4l-cx231xx-avcore-01.fw WHENCE
`linux-firmware-20220913.tar.xz`_ meson-vdec - Amlogic video decoder                 meson/vdec/\*            LICENSE.amlogic_vdec, WHENCE
`linux-firmware-20220913.tar.xz`_ lt9611uxc - Lontium DSI to HDMI bridge             lt9611uxc_fw.bin         LICENSE.Lontium, WHENCE
================================= ================================================== ======================== ============================

Some other firmware files are covered by proprietary licenses that contain
termination clauses providing that either party may terminate the license at
any time without cause, which may work as killswitches (i.e. vendor may
terminate your license at any time without any reason, so your devices -
including already distributed ones - may lose, say, Bluetooth or Wifi support).
You should seek legal advice (and possibly negotiate a different license with
the vendor) if you need to use the affected firmware files:

========================================================== ====================== ======== ================
Source                                                     Device/driver          File(s)  License found in
========================================================== ====================== ======== ================
[git://github.com/murata-wireless/cyw-fmac-fw@ba140e42]    Murata Wi-Fi/Bluetooth cyfmac\* LICENCE, README
[git://github.com/murata-wireless/cyw-fmac-nvram@8710e74e] Murata Wi-Fi/Bluetooth cyfmac\* LICENCE.cypress
[git://github.com/murata-wireless/cyw-bt-patch@9d040c25]   Broadcom BCM43455 Wifi \*.hcd   LICENCE.cypress
========================================================== ====================== ======== ================

Some other firmware files (for NVIDIA hardware, that is not included in any of
Oniro’s supported boards) have been expressly excluded from installation
because they come with a proprietary license with an unclear “open source
exception”. See `issue #834`_ in Oniro main repo for further details.

Some other firmware files are covered by a limited patent license. If you need
to use them, you should check whether you fulfill the conditions of such a
license.

================================= ========================= ============================= ======================
Source                            Device/driver             File(s)                       License found in
================================= ========================= ============================= ======================
`linux-firmware-20220913.tar.xz`_ WiLink4 chips WLAN driver ti-connectivity/wl1251-fw.bin LICENCE.wl1251, WHENCE
================================= ========================= ============================= ======================

Finally, some licenses have unclear license wording about use and
redistribution. If you need to use firmware covered by such files, you should
check and possibly seek legal advice.

================================= ===================================================== ================================== =======================
Source                            Device/driver                                         File(s)                            License found in
================================= ===================================================== ================================== =======================
`linux-firmware-20220913.tar.xz`_ WiLink4 chips WLAN driver                             ti-connectivity/wl1251-fw.bin      LICENCE.wl1251, WHENCE
`linux-firmware-20220913.tar.xz`_ Marvell Libertas 802.11b/g cards                      libertas/\*.bin, mrvk/\*.bin       LICENCE.Marvell, WHENCE
`linux-firmware-20220913.tar.xz`_ Marvell mac80211 driver for 80211ac cards             mwlwifi/\*.bin                     LICENCE.Marvell, WHENCE
`linux-firmware-20220913.tar.xz`_ Marvell CPT driver                                    mrvl/cpt01/\*                      LICENCE.Marvell, WHENCE
`linux-firmware-20220913.tar.xz`_ Marvell driver for Prestera family ASIC devices       mrvl/prestera/\*.img               LICENCE.Marvell, WHENCE
`linux-firmware-20220913.tar.xz`_ wave5 - Chips&Media, Inc. video codec driver          cnm/wave521c_j721s2_codec_fw.bin   LICENCE.cnm, WHENCE
`linux-firmware-20220913.tar.xz`_ Broadcom 802.11n fullmac wireless LAN driver          brcm/brcmfmac/\*, cypress/cyfmac\* LICENCE.cypress, WHENCE
`linux-firmware-20220913.tar.xz`_ BCM-0bb4-0306 Cypress Bluetooth firmware for HTC Vive brcm/BCM-0bb4-0306.hcd             LICENCE.cypress, WHENCE
================================= ===================================================== ================================== =======================

Zephyr
~~~~~~

The third-party repository ‘`zephyr-philosophers`_’ fetched by
zephyr-philosophers recipe contains many sub-components for specific hardware
support, coming from different hardware vendors. Some of them have specific
proprietary license conditions (eg. software components to support Atmel SAM
L21, Altera Nios II, Cypress/Infineon PSoC6) but are not used to generate Oniro
images, so they are not covered here. Should you need to add support for such
hardware boards, not officially supported by Oniro, you should carefully check
hardware vendor's license conditions.

Warnings for Downstream Users: Patents
--------------------------------------

“Dropbear” component documentation contains a patent and trademark notice:

   The author (Tom St Denis) is not a patent lawyer so this section is not to
   be treated as legal advice. To the best of the author’s knowledge, the only
   patent-related issues within the library are the RC5 and RC6 symmetric block
   cyphers. They can be removed from a build by simply commenting out the two
   appropriate lines in `\textit{tomcrypt\_custom.h}`. The rest of the cyphers
   and hashes are patent-free or under patents that have since expired.

   The RC2 and RC4 symmetric cyphers are not under patents but are under
   trademark regulations. This means you can use the cyphers you just can’t
   advertise that you are doing so.

To our best knowledge, also patents on RC5 and RC6 symmetric block cyphers have
expired, but you should seek legal advice to check whether there still are
active patents covering such technologies.

.. [*]
   Carlo Piana and Alberto Pianon from Array (Legal); Rahul Mohan G. and
   Vaishali Avhad from NOI Techpark (Audit)
.. [*] SBOM  is short for Software Bill Of Material, the full and detailed list
   of upstream components. SPDX is short for Software Package Data Exchange, an
   `ISO standard <https://spdx.github.io/spdx-spec>`_ to communicate
   information about software in a machine-readable form.

.. _toolchain: https://projects.eclipse.org/projects/oniro.oniro-compliancetoolchain
.. _process: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/docs/-/tree/main/audit_workflow
.. _Fossology: https://www.fossology.org
.. _Scancode: https://nexb.com/scancode
.. _official documentation: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/docs
.. _tinfoilhat: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/tinfoilhat
.. _aliens4friends: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/aliens4friends
.. _dashboard: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/dashboard
.. _pipelines: https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/toolchain/pipelines
.. _EULA: https://git.yoctoproject.org/meta-freescale/tree/EULA
.. _linux-firmware-20220913.tar.xz: https://cdn.kernel.org/pub/linux/kernel/firmware/linux-firmware-20220913.tar.xz
.. _issue #834: https://gitlab.eclipse.org/eclipse/oniro-core/oniro/-/issues/834
.. _zephyr-philosophers: https://github.com/zephyrproject-rtos/zephyr
.. _dedicated dashboard: https://sca.software.bz.it/?json=https://gitlab.eclipse.org/eclipse/oniro-compliancetoolchain/mirrors/oniro-goofy/-/jobs/artifacts/kirkstone/raw/report.harvest.json?job=harvest
