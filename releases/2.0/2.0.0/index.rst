.. SPDX-FileCopyrightText: Huawei Inc.
..
.. SPDX-License-Identifier: CC-BY-4.0

.. include:: ../../../definitions.rst

2.0.0
#####

**Release timeframe**: 2022-03-07 .. 2022-11-30

**Release Artefacts Download Area**: https://download.eclipse.org/oniro-core/releases/2.0.0/

**Release Tags GPG Public Key**: https://download.eclipse.org/oniro-core/releases/2.0.0/oniro-2.0.0_gpg_key.asc [*]_

.. toctree::
   :maxdepth: 1
   
   release_notes
   requirements
   test_report
   ip_compliance_note
   security_report

.. [*] All repositories released part of 2.0.0 have associated 2.0.0 `git
   tags` that have been signed during the Eclipse Foundation release process.
   You can use this GPG public key to verify all these signatures.
