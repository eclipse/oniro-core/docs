.. SPDX-FileCopyrightText: Luca Favaretto <luca.favaretto@kalpa.it> and Luca zizolfi <luca.zizolfi@kalpa.it>
..
.. SPDX-License-Identifier: CC-BY-4.0

.. _2_0_0_TestReport:

Test Report
==================

Results by test suite
-----------------------

+---------------------+-------------------+-------+---------+---------+---------+----------+------+-----------+
| TESTS               | Manual Automated  | TEST  | NO RUN  | PASSED  | FAILED  | BLOCKED  | N/A  | SKIPPED   |
+=====================+===================+=======+=========+=========+=========+==========+======+===========+
| Kernel Build        | Both              | 32    | 0       | 24      | 6       |          | 2    |           |
+---------------------+-------------------+-------+---------+---------+---------+----------+------+-----------+
| OTA                 | Automated         | 6     | 0       | 6       |         |          |      |           |
+---------------------+-------------------+-------+---------+---------+---------+----------+------+-----------+
| Networking          | Manual            | 2     | 0       |         |         | 1        |      | 1         |
+---------------------+-------------------+-------+---------+---------+---------+----------+------+-----------+
| Reference Hardware  | Manual            | 48    | 0       | 10      |         | 29       | 5    | 4         |
+---------------------+-------------------+-------+---------+---------+---------+----------+------+-----------+
| Security            | Manual            | 170   | 0       | 157     |         |          | 13   |           |
+---------------------+-------------------+-------+---------+---------+---------+----------+------+-----------+
| Podman              | Automated         | 1300  | 0       | 234     | 960     |          | 106  |           |
+---------------------+-------------------+-------+---------+---------+---------+----------+------+-----------+
| LTP                 | Automated         | 15030 | 0       | 12381   | 520     |          | 2129 |           |
+---------------------+-------------------+-------+---------+---------+---------+----------+------+-----------+
| Kernel self test    | Automated         | 56    | 0       | 8       | 24      |          | 24   |           |
+---------------------+-------------------+-------+---------+---------+---------+----------+------+-----------+
| Libc                | Automated         | 2185  | 0       | 1464    | 721     |          |      |           |
+---------------------+-------------------+-------+---------+---------+---------+----------+------+-----------+

Overall Results
-----------------

+--------------------+------------+
| **Total**          | **23442**  |
+--------------------+------------+
|                    |            |
+--------------------+------------+
| *Passed*           | *17994*    |
+--------------------+------------+
| *Failed*           | *2324*     |
+--------------------+------------+
| *Blocked*          | *30*       |
+--------------------+------------+
| *Skipped*          | *5*        |
+--------------------+------------+
| *Out of scope*     | *3089*     |
+--------------------+------------+
| *No Run*           | *0*        |
+--------------------+------------+
|                    |            |
+--------------------+------------+
| **TEST PASS RATE** | **89,94%** |
+--------------------+------------+
| % Blocked          | 0,13%      |
+--------------------+------------+
| % Failed           | 9,91%      |
+--------------------+------------+
| % Skipped          | 0,02%      |
+--------------------+------------+
